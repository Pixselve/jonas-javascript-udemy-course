// Scores

// John's Team
var john1 = 89;
var john2 = 120;
var john3 = 94;

// Mike's Team
var mike1 = 116;
var mike2 = 94;
var mike3 = 123;

// Mary's Team
var mary1 = 97;
var mary2 = 134;
var mary3 = 105;

// ==========
// Average scores
// var johnAVG = (john1 + john2 + john3) / 3;
// var mikeAVG = (mike1 + mike2 + mike3) / 3;
// var maryAVG = (mary1 + mary2 + mary3) / 3;
var johnAVG = 10;
var mikeAVG = 5;
var maryAVG = 10;

// ==========
// Winner

switch (true) {
  case johnAVG > mikeAVG && johnAVG > maryAVG:
    console.log("John's Team win ! with an average score of " + johnAVG);
    break;

  case mikeAVG > johnAVG && mikeAVG > maryAVG:
    console.log("Mike's Team win ! with an average score of " + mikeAVG);
    break;

  case maryAVG > johnAVG && maryAVG > mikeAVG:
    console.log("Mary's Team win ! with an average score of " + maryAVG);
    break;

  case johnAVG === mikeAVG && mikeAVG > maryAVG:
    console.log("John's Team and Mike's Team wins");
    break;

  case maryAVG === johnAVG && johnAVG > mikeAVG:
    console.log("Mary's Team and John's Team wins");
    break;
  case maryAVG === mikeAVG && mikeAVG > johnAVG:
    console.log("Mary's Team and Mike's Team wins");
    break;

  default:
    console.log('All scores the same :/');
    break;
}
