// Objects
Mark = {
  fullName: 'Mark Smith',
  mass: 79,
  height: 1.75,
  BMI: undefined,
  BMICalc: function() {
    var BMI = this.mass / (this.height * this.height);
    this.BMI = BMI;
    return BMI;
  }
};
John = {
  fullName: 'John Smith',
  mass: 82,
  height: 1.86,
  BMI: undefined,
  BMICalc: function() {
    var BMI = this.mass / (this.height * this.height);
    this.BMI = BMI;
    return BMI;
  }
};

function highestBMI() {
  Mark.BMICalc();
  John.BMICalc();

  if (Mark.BMI > John.BMI) {
    return (
      Mark.fullName +
      ' has a higher BMI than ' +
      John.fullName +
      '( ' +
      Mark.BMI +
      ' > ' +
      John.BMI +
      ' ).'
    );
  } else if (John.BMI > Mark.BMI) {
    return (
      John.fullName +
      ' has a higher BMI than ' +
      Mark.fullName +
      '( ' +
      John.BMI +
      ' > ' +
      Mark.BMI +
      ' ).'
    );
  } else {
    return (
      John.fullName + Mark.fullName + ' have the same BMI ( ' + John.BMI + ' ).'
    );
  }
}

console.log(highestBMI());
