var bills = [124, 48, 268];

function tipCalculator(bill) {
  if (bill < 50) {
    return bill * 0.2;
  } else if (bill < 200) {
    return bill * 0.15;
  } else {
    return bill * 0.1;
  }
}

var tips = new Array();

tips.push(tipCalculator(bills[0]));
tips.push(tipCalculator(bills[1]));
tips.push(tipCalculator(bills[2]));

var finalPaid = new Array();

finalPaid.push(bills[0] + tipCalculator(bills[0]));
finalPaid.push(bills[1] + tipCalculator(bills[1]));
finalPaid.push(bills[2] + tipCalculator(bills[2]));

console.log(tips, finalPaid);
